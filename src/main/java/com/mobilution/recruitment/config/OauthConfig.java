package com.mobilution.recruitment.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableOAuth2Sso
@Configuration
public class OauthConfig extends WebSecurityConfigurerAdapter{	
	
	
	
	
	
	@Value("${security.oauth2.custom.server-logout-url}") private String serverLogoutUrl;

    @Value("${security.oauth2.custom.server-logouted-redirect-url}") private String serverLogoutedRedirectUrl;
	  
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
        		.antMatcher("/**")
                .authorizeRequests()
                .antMatchers("/", "/login**")                
                .permitAll()
                .anyRequest()
                .authenticated()
                
               /* .and()
				.formLogin()
				.loginPage("http://localhost:9018/auth/login?logout")
				.defaultSuccessUrl(serverLogoutedRedirectUrl)
                */
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("http://localhost:9018/auth/exit")
            //    .logoutSuccessUrl(serverLogoutUrl + "?next=" + serverLogoutedRedirectUrl)
                //.logoutSuccessHandler(customLogoutSuccessHandler)            
             //  .logoutSuccessUrl(serverLogoutUrl + "?next=" + serverLogoutedRedirectUrl)
                  .deleteCookies("RECRUITSESSION")
                  .invalidateHttpSession(true)
                  .permitAll()
              ;

    }
}