package com.mobilution.recruitment.job;

import java.io.IOException;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.mobilution.recruitment.candidate.Candidate;
import com.mobilution.recruitment.candidate.CandidateService;
import com.mobilution.recruitment.client.Client;
import com.mobilution.recruitment.client.ClientContactService;
import com.mobilution.recruitment.client.ClientService;

@Controller
public class JobController {

	@Autowired
	private JobService jobService;

	@Autowired
	private ClientService clientService;

	@Autowired
	private CandidateService candidateService;

	/*@Autowired
	private ClientContactService clientContactService;*/

	@RequestMapping("/homeview")
	String home() {
		return "home";
	}

	@RequestMapping(value = "/jobview", method = RequestMethod.GET)
	public String jobView(Model model) {
		System.out.println("entered jobview Controller");

		model.addAttribute("jobFormObj", new Job());
		model.addAttribute("clientService", new ClientService());

		model.addAttribute("clientContactService",new ClientContactService());

		model.addAttribute("jobs", jobService.alljobs());
		model.addAttribute("clients", clientService.allClients());
		System.out.println("CLIENT NAME"+clientService.getClientNameByClientId(3));

		return "jobform";
	}

	@RequestMapping(value = "/addjob", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addjob(Model model, Job job, Client client) {

		System.out.println("Entered addjob Controller");
		model.addAttribute("client", new Client());
		if ("employee".equalsIgnoreCase(job.getJobType())) {
			job.setDuration("N/A");
		}
		jobService.save(job);

		return "redirect:/getalljobs";
	}

	
	@RequestMapping(value = "/appliedjobcandidates", method = RequestMethod.GET)
	public String appliedJobCandidates(Model model,Job job, Client client) {
	
		System.out.println("Entered aapplied job candidates Controller");
		model.addAttribute("client", new Client());
		getJobDetails(model);
		return "jobcandidates";
	}
	
	
	@RequestMapping(value = "/alljobcandidateform/{jobId}", method = RequestMethod.GET)
	public String allJobCanddiateForm(Model model, @PathVariable("jobId") Integer jobId) {

		System.out.println("Entered alljobcandidateform Controller");

		Job job = jobService.getByjobId(jobId);
		model.addAttribute("candidates", job.getCandidates());
		model.addAttribute("job", job);

		return "alljobcandidateform";
	}

	@RequestMapping(value = "/deletejobcandidate/{candidateId}/{jobId}", method = RequestMethod.GET)
	public String delteJobCandidate(Model model, @PathVariable("jobId") Integer jobId,
			@PathVariable("candidateId") Integer candidateId) {

		System.out.println("Entered deletejobcandidate Controller");
		Job job = jobService.getByjobId(jobId);
		Candidate candidate = candidateService.getByCandidateId(candidateId);
		Set<Job> jobAs = candidate.getJobs();
		jobAs.remove(job);

		candidate.setJobs(jobAs);
		candidateService.save(candidate);

		model.addAttribute("candidates", job.getCandidates());
		model.addAttribute("job", job);

		if (job.getCandidates().size() > 0)
			return "alljobcandidateform";
		else
			return "redirect:/getalljobs";
	}

	@RequestMapping(value = "/jobcandidateform/{jobId}", method = RequestMethod.GET)
	public String addJobCandidateForm(Model model, @PathVariable("jobId") Integer jobId) {

		System.out.println("Entered jobcandidateform Controller");
		Job job = jobService.getByjobId(jobId);
		model.addAttribute("candidates", candidateService.allCandidates());
		model.addAttribute("job", job);
		return "jobcandidateform";
	}

	@RequestMapping(value = "/addjobcandidate/{candidateId}/{jobId}", method = RequestMethod.GET)
	public String addJobCandidate(Model model, @PathVariable("jobId") Integer jobId,
			@PathVariable("candidateId") Integer candidateId) {

		Job job = jobService.getByjobId(jobId);
		Candidate candidate = candidateService.getByCandidateId(candidateId);
		Set<Job> jobAs = candidate.getJobs();
		jobAs.add(job);
		candidate.setJobs(jobAs);
		candidateService.save(candidate);
		return "redirect:/getalljobs";
	}

	@RequestMapping(value = "/jobupdateform/{jobId}", method = RequestMethod.GET)
	public String canddiateUpdateForm(Model model, Client client, @PathVariable("jobId") Integer jobId) {
		model.addAttribute("job", jobService.getByjobId(jobId));
		model.addAttribute("clients", clientService.allClients());
		return "updatejobform";
	}

	@RequestMapping(value = "/getalljobs", method = RequestMethod.GET)
	public String getJobDetails(Model model) {
		model.addAttribute("jobFormObj", new Job());
		model.addAttribute("clientService", new ClientService());
		model.addAttribute("clientContactService", new ClientContactService());
		model.addAttribute("clients", clientService.allClients());
		model.addAttribute("jobs", jobService.alljobs());
		return "jobform";
	}

	@RequestMapping(value = "/deletejob/{jobId}", method = RequestMethod.GET)
	public String deletejob(Model model, @PathVariable("jobId") Integer jobId) throws IOException {

		System.out.println("Entered deletejob Controller");
		Job job = jobService.getByjobId(jobId);

		Set<Candidate> candidateAs = job.getCandidates();

		for (Candidate candidate : candidateAs) {
			Set<Job> jobAs = candidate.getJobs();
			jobAs.remove(job);

			candidate.setJobs(jobAs);
			candidateService.save(candidate);
		}

		jobService.deleteByjobId(jobId);

		return "redirect:/getalljobs";
	}
}