package com.mobilution.recruitment.job;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class JobService {

	@Autowired
	private JobRepository jobRepository;

	public void save(Job cleintDetails) {
		jobRepository.save(cleintDetails);
	}

	public void update(int jobId, Job jobDetails) {
		jobRepository.save(jobDetails);
	}

	public List<Job> alljobs() {
		return jobRepository.findAll();
	}

	public void deleteByjobId(Integer jobId) {
		jobRepository.deleteByJobId(jobId);
	}

	public Job getByjobId(Integer jobId) {
		return jobRepository.getByJobId(jobId);
	}
}