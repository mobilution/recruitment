package com.mobilution.recruitment.job;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.mobilution.recruitment.candidate.Candidate;



@Entity
@Table(name = "job")
public class Job {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "job_id")
	private int jobId;

	@Column(name = "job_title", length = 50)
	private String jobTitle;

	@Column(name = "job_description", length = 500)
	private String jobDescription;

	@Column(name = "job_location", length = 40)
	private String jobLocation;

	@Column(name = "job_type", length = 30)
	private String jobType;

	@Column(name = "duration", length = 50)
	private String duration;

	@Column(name = "budget_min", length = 10)
	private String budgetMin;

	@Column(name = "budget_max", length = 10)
	private String budgetMax;

	@Column(name = "mandatory_skills", length = 500)
	private String mandatorySkills;

	@Column(name = "desired_skills", length = 500)
	private String desiredSkills;

	@Column(name = "client_id", length = 10)
	private String clientId;

	@Column(name = "client_contact_id", length = 50)
	private String clientContactId;

	@Column(name = "client_contact_email", length = 40)
	private String clientContactEmail;

	@Column(name = "client_contact_number", length = 20)
	private String clientContactNumber;

	@Column(name = "status", length = 20)
	private String status;

	@ManyToMany(mappedBy = "jobs")
	private Set<Candidate> candidates;
	
	public Job() { 

	}

	public Job(int jobId, String jobTitle, String jobDescription, String jobLocation, String jobType, String duration,
			String budgetMin, String budgetMax, String mandatorySkills, String desiredSkills, String clientId,
			String clientContactId, String clientContactEmail, String clientContactNumber, String status) {
		super();
		this.jobId = jobId;
		this.jobTitle = jobTitle;
		this.jobDescription = jobDescription;
		this.jobLocation = jobLocation;
		this.jobType = jobType;
		this.duration = duration;
		this.budgetMin = budgetMin;
		this.budgetMax = budgetMax;
		this.mandatorySkills = mandatorySkills;
		this.desiredSkills = desiredSkills;
		this.clientId = clientId;
		this.clientContactId = clientContactId;
		this.clientContactEmail = clientContactEmail;
		this.clientContactNumber = clientContactNumber;
		this.status = status;
	}

	public Job(int jobId, String jobTitle, String jobDescription, String jobLocation, String jobType, String duration,
			String budgetMin, String budgetMax, String mandatorySkills, String desiredSkills, String clientId,
			String clientContactId, String clientContactEmail, String clientContactNumber, String status,Set<Candidate> candidates) {
		super();
		this.jobId = jobId;
		this.jobTitle = jobTitle;
		this.jobDescription = jobDescription;
		this.jobLocation = jobLocation;
		this.jobType = jobType;
		this.duration = duration;
		this.budgetMin = budgetMin;
		this.budgetMax = budgetMax;
		this.mandatorySkills = mandatorySkills;
		this.desiredSkills = desiredSkills;
		this.clientId = clientId;
		this.clientContactId = clientContactId;
		this.clientContactEmail = clientContactEmail;
		this.clientContactNumber = clientContactNumber;
		this.status = status;
		this.candidates=candidates;
	}
	
	public int getJobId() {
		return jobId;
	}

	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getBudgetMin() {
		return budgetMin;
	}

	public void setBudgetMin(String budgetMin) {
		this.budgetMin = budgetMin;
	}

	public String getBudgetMax() {
		return budgetMax;
	}

	public void setBudgetMax(String budgetMax) {
		this.budgetMax = budgetMax;
	}

	public String getMandatorySkills() {
		return mandatorySkills;
	}

	public void setMandatorySkills(String mandatorySkills) {
		this.mandatorySkills = mandatorySkills;
	}

	public String getDesiredSkills() {
		return desiredSkills;
	}

	public void setDesiredSkills(String desiredSkills) {
		this.desiredSkills = desiredSkills;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	

	public String getClientContactId() {
		return clientContactId;
	}

	public void setClientContactId(String clientContactId) {
		this.clientContactId = clientContactId;
	}

	public String getClientContactEmail() {
		return clientContactEmail;
	}

	public void setClientContactEmail(String clientContactEmail) {
		this.clientContactEmail = clientContactEmail;
	}

	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClientContactNumber() {
		return clientContactNumber;
	}

	public void setClientContactNumber(String clientContactNumber) {
		this.clientContactNumber = clientContactNumber;
	}

	public Set<Candidate> getCandidates() {
		return candidates;
	}

	public void setCandidates(Set<Candidate> candidates) {
		this.candidates = candidates;
	}

}