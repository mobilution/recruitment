package com.mobilution.recruitment.job;

import java.util.List;

import org.springframework.data.repository.CrudRepository;



public interface JobRepository extends CrudRepository<Job, Integer>{
	
	List<Job> findAll();
	
	void deleteByJobId(Integer jobId);	
	
	Job getByJobId(Integer jobd);
	
	//Set<Candidate> findByCandidateNotIn(Job job);

}
