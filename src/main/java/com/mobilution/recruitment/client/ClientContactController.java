package com.mobilution.recruitment.client;

import java.io.IOException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ClientContactController {

	@Autowired
	private ClientContactService clientContactService;

	@Autowired
	private ClientService clientService;

	@RequestMapping(value = "/clientcontactform/{clientId}", method = RequestMethod.GET)
	public String clientContactForm(Model model, @PathVariable("clientId") Integer clientId) {

		System.out.println("Entered Add Controller");

		model.addAttribute("clientContact", new ClientContact());
		model.addAttribute("clientId", clientId);

		return "clientcontactform";
	}

	@RequestMapping(value = "/clientcontacts", method = RequestMethod.GET)
	@ResponseBody
	public Set<ClientContact> getClientContacts(Model model, @RequestParam(value = "clientId") Integer clientId) {

		System.out.println("Entered Add Controller");
		System.out.println("SIZE" + clientService.getByClientId(clientId).getClientContacts().size());
        Set<ClientContact> clientContacts = clientService.getByClientId(clientId).getClientContacts();
		return clientContacts;
	}
	
	@RequestMapping(value = "/clientcontactdetail", method = RequestMethod.GET)
	@ResponseBody
	public ClientContact getClientContactDetail(Model model, @RequestParam(value = "contactId") Integer contactId) {

		return clientContactService.getClientContactByContactId(contactId);
	}

	@RequestMapping(value = "/addclientcontact", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addClient(Model model, @RequestParam(value = "clientId") Integer clientId,
			ClientContact clientContact) {

			System.out.println("Entered Add Controller");
		    clientContactService.save(clientId, clientContact);
		    return "redirect:/getallclients";
	}

	@RequestMapping(value = "/clientcontactupdateform/{contactId}/{clientId}", method = RequestMethod.GET)
	public String clientUpdate(Model model, @PathVariable("contactId") Integer contactId,
			@PathVariable("clientId") Integer clientId) {

		ClientContact clientContact = clientContactService.getClientContactByContactId(contactId);
        model.addAttribute("clientContact", clientContact);
		model.addAttribute("client", clientService.getByClientId(clientId));
		return "updateclientcontactform";
	}

	@RequestMapping(value = "/updateclientcontact", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String updateClient(Model model, @RequestParam(value = "contactId") Integer contactId,
			@RequestParam(value = "clientId") Integer clientId, ClientContact clientContact) {

		System.out.println("Entered Update Controller");

		System.out.println("CLIENT CONTCT ID" + clientContact.getClientContactName());
		System.out.println("CLIENT CONTCT ID" + contactId);
		clientContactService.update(contactId, clientContact);
		Client client = clientService.getByClientId(clientId);
		model.addAttribute("clientcontacts", client.getClientContacts());
		model.addAttribute("client", client);
		if (client.getClientContacts().size() > 0)
			return "clientcontactlist";
		else
			return "redirect:/getallclients";

	}

	@RequestMapping(value = "/getallclientcontacts", method = RequestMethod.GET)
	public String getClentDetails(Model model) {
		model.addAttribute("clientcontacts", clientContactService.allClientContacts());
		if (clientContactService.allClientContacts().size() > 0)
			return "clientcontactlist";
		else
			return "redirect:/getallclients";
	}

	@RequestMapping(value = "/getallclientcontacts/{clientId}", method = RequestMethod.GET)
	public String getClentDetails(Model model, @PathVariable("clientId") Integer clientId) {
		Client client = clientService.getByClientId(clientId);
		model.addAttribute("clientcontacts", client.getClientContacts());
		model.addAttribute("client", client);
		if (client.getClientContacts().size() > 0)
			return "clientcontactlist";
		else
			return "redirect:/getallclients";
	}

	@RequestMapping(value = "/deleteclientcontact/{contactId}", method = RequestMethod.GET)
	public String deleteClient(Model model, @PathVariable("contactId") Integer contactId) throws IOException {
		System.out.println("Entered Delete Controller");

		ClientContact clientContact = clientContactService.getClientContactByContactId(contactId);
		Client client = clientContact.getClient();

		clientContactService.deleteByContactId(contactId);

		System.out.println("SIZEEEEEEEEE" + clientContactService.allClientContacts().size());
		model.addAttribute("clientcontacts", client.getClientContacts());

		model.addAttribute("client", client);

		if (client.getClientContacts().size() > 0)

			return "clientcontactlist";

		else
			return "redirect:/getallclients";
	}
}