package com.mobilution.recruitment.client;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ClientContactRepository  extends CrudRepository<ClientContact, Integer> {

	List<ClientContact> findAll();
	
	void deleteByContactId(Integer contactId);
	
	ClientContact getClientContactByContactId(Integer contactId);
	
	//String getClientContactNameByContactId(Integer contactId);
	
	
	@Query(value ="select c.client_contact_name from clientcontact c where c.contact_id = :contactId", nativeQuery = true )
	public String getClientContactNameByContactId(@Param("contactId") Integer contactId);
	
}
