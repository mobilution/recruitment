package com.mobilution.recruitment.client;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "clientcontact")
public class ClientContact {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "contact_id")
	private int contactId;

	@Column(name = "client_contact_name", length = 100)
	private String clientContactName;

	@Column(name = "client_title", length = 100)
	private String clientTitle;

	@Column(name = "client_email", length = 45)
	private String clientEmail;

	@Column(name = "client_phone_no", length = 20)
	private String clientPhoneNo;

	@Column(name = "client_office_no", length = 20)
	private String clientOfficeNo;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "client_contact_id")
	private Client client;

	//private ClientContact clientContact; 
	
	public ClientContact() {

	}

	/*public ClientContact(ClientContact clientContact,Client client) {
		this.clientContact=clientContact;
		this.client = client;	

	}*/
	
	public ClientContact(String clientContactName, String clientTitle, String clientEmail, String clientPhoneNo,
			String clientOfficeNo, Client client) {
		super();
		
		this.clientContactName = clientContactName;
		this.clientTitle = clientTitle;
		this.clientEmail = clientEmail;
		this.clientPhoneNo = clientPhoneNo;
		this.clientOfficeNo = clientOfficeNo;
		this.client = client;
	}


	public String getClientContactName() {
		return clientContactName;
	}

	public void setClientContactName(String clientContactName) {
		this.clientContactName = clientContactName;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	
	public int getClientContactId() {
		return contactId;
	}

	public void setClientContactId(int clientContactId) {
		this.contactId = clientContactId;
	}

	public String getClientTitle() {
		return clientTitle;
	}

	public void setClientTitle(String clientTitle) {
		this.clientTitle = clientTitle;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmai) {
		this.clientEmail = clientEmai;
	}

	public String getClientPhoneNo() {
		return clientPhoneNo;
	}

	public void setClientPhoneNo(String clientPhoneNo) {
		this.clientPhoneNo = clientPhoneNo;
	}

	public String getClientOfficeNo() {
		return clientOfficeNo;
	}

	public void setClientOfficeNo(String clientOfficeNo) {
		this.clientOfficeNo = clientOfficeNo;
	}

}