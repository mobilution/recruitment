package com.mobilution.recruitment.client;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClientContactService {

	@Autowired
	private ClientContactRepository clientContactRepository;

	@Autowired
	private ClientRepository clientRepository;

	public void save(ClientContact cleintDetails) {
		clientContactRepository.save(cleintDetails);
	}

	public void save(Integer clientId, ClientContact cleintContactDetails) {

		Client client = clientRepository.getByClientId(clientId);

		Set<ClientContact> clientContactAs = new HashSet<ClientContact>() {
			{
				add(new ClientContact(cleintContactDetails.getClientContactName(), cleintContactDetails.getClientTitle(),
						cleintContactDetails.getClientEmail(), cleintContactDetails.getClientPhoneNo(),
						cleintContactDetails.getClientOfficeNo(), client));
			}
		};

		client.setClientContacts(clientContactAs);

		clientRepository.save(client);

	}

	public void update(int contactId, ClientContact cleintContactDetails) {
		clientContactRepository.save(cleintContactDetails);
	}

	public List<ClientContact> allClientContacts() {
		return clientContactRepository.findAll();
	}

	public void deleteByContactId(Integer clientId) {
		clientContactRepository.deleteByContactId(clientId);
	}
	
	
	public String getClientContactNameByContactId(Integer contactId) {
		return clientContactRepository.getClientContactNameByContactId(contactId);
	}

	
	public ClientContact getClientContactByContactId(Integer contactId) {
		return clientContactRepository.getClientContactByContactId(contactId);
	}

}
