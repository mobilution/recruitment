package com.mobilution.recruitment.client;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "client_id")
	private int clientId;	

	@Column(name = "client_name", length = 100)
	private String clientName;
	
	@Column(name = "client_location", length = 100)
	private String clientLocation;
	
	 @OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
	private Set<ClientContact> clientContacts;

	public Client()
	{
		
	}
	public Client(int clientId, String clientName, String clientLocation) {
		super();
		this.clientId = clientId;
		this.clientName = clientName;
		this.clientLocation = clientLocation;
	}

	public Client(String clientName, String clientLocation) {
		super();
		this.clientName = clientName;
		this.clientLocation = clientLocation;
	}
	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientLocation() {
		return clientLocation;
	}

	public void setClientLocation(String clientLocation) {
		this.clientLocation = clientLocation;
	}
	public Set<ClientContact> getClientContacts() {
		return clientContacts;
	}
	public void setClientContacts(Set<ClientContact> clientContacts) {
		this.clientContacts = clientContacts;
	}	
	
}