package com.mobilution.recruitment.client;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;

	public void save(Client cleintDetails) {
		clientRepository.save(cleintDetails);
	}

	public void update(int clientId, Client cleintDetails) {
		clientRepository.save(cleintDetails);
	}

	
	public List<Client> allClients() {
		return clientRepository.findAll();
	}

	public void deleteByClientId(Integer clientId) {
		clientRepository.deleteClientContactByClientId(clientId);
	}

	public Client getByClientId(Integer clientId) {
		return clientRepository.getByClientId(clientId);
	}
	
	public Client getClientByClientId(Integer clientId) {
		return clientRepository.getClientByClientId(clientId);
	}
	
	public String getClientNameByClientId(Integer clientId) {
		return clientRepository.getClientNameByClientId(clientId);
	}
	
	public List<ClientContact> getClientContactByClientId(Integer clientId) {
		return clientRepository.getClientContactByClientId(clientId);
	}
}