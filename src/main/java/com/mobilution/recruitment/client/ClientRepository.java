package com.mobilution.recruitment.client;

import java.util.List;


import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends CrudRepository<Client, Integer> {

	List<Client> findAll();

	void deleteByClientId(Integer clientId);

	Client getByClientId(Integer clientId);

	Client getClientByClientId(Integer clientId);

	void deleteClientContactByClientId(Integer clientId);

	List<ClientContact> getClientContactByClientId(Integer clientId);


//	String getClientNameByClientId(Integer clientId);
	
	@Query(value ="select c.client_name from client c where c.client_id = :clientId", nativeQuery = true )
	public String getClientNameByClientId(@Param("clientId") Integer clientId);


}
