package com.mobilution.recruitment.client;

import java.io.IOException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ClientController {

	@Autowired
	private ClientService clientService;

	private Client client;

	@RequestMapping("/")
	String home() {
		return "home";
	}

	@RequestMapping(value = "/client", method = RequestMethod.GET)
	public String Client(Model model) {
		System.out.println("entered the client method");

		model.addAttribute("client", new Client());
		return "redirect:/getallclients";
	}

	@RequestMapping(value = "/addclient", method = RequestMethod.POST)
	public String addClient(Model model, @RequestParam("clientName") String clientName,
			@RequestParam("clientLocation") String clientLocation) {

		System.out.println("Entered Add Controller");
		client = new Client();
		client.setClientName(clientName);
		client.setClientLocation(clientLocation);
		clientService.save(client);

		return "redirect:/getallclients";
	}

	@RequestMapping(value = "/clientupdateform/{clientId}", method = RequestMethod.GET)
	public String clientUpdate(Model model, @PathVariable("clientId") Integer clientId) {

		Client client = clientService.getByClientId(clientId);
		model.addAttribute("client", client);
		return "updateclientform";
	}

	@RequestMapping(value = "/updateclient", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String updateClient(Model model, Client client) {

		System.out.println("Entered Update Controller");

		clientService.update(client.getClientId(), client);

		return "redirect:/getallclients";
	}

	@RequestMapping(value = "/getallclients", method = RequestMethod.GET)
	public String getClentDetails(Model model) {
		model.addAttribute("clients", clientService.allClients());
		return "clientform";
	}
	
	@RequestMapping(value = "/getallclientlist", method = RequestMethod.GET)
	@ResponseBody
	public Set<ClientContact> getClientContacts(Model model, @RequestParam(value = "clientId") Integer clientId) {

		System.out.println("Entered getallclientlist Controller");
		System.out.println("SIZE" + clientService.getByClientId(clientId).getClientContacts().size());

		Set<ClientContact> clientContacts = clientService.getByClientId(clientId).getClientContacts();
		return clientContacts;
	}

	@RequestMapping(value = "/deleteclient/{clientId}", method = RequestMethod.GET)
	public String deleteClient(Model model, @PathVariable("clientId") Integer clientId) throws IOException {
		System.out.println("Entered Delete Controller");

		clientService.deleteByClientId(clientId);

		return "redirect:/getallclients";
	}
}