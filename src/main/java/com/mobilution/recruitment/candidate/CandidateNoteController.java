package com.mobilution.recruitment.candidate;

import java.io.IOException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CandidateNoteController {

	@Autowired
	private CandidateNoteService candidateNoteService;

	@Autowired
	private CandidateService candidateService;

	@RequestMapping(value = "/candidatenoteform/{candidateId}", method = RequestMethod.GET)
	public String candidateNote(Model model, @PathVariable("candidateId") Integer candidateId) {
		System.out.println("entered the candidateNote method");

		model.addAttribute("candidateNote", new CandidateNote());
		model.addAttribute("candidateId", candidateId);

		return "candidatenoteform";
	}

	@RequestMapping(value = "/candidatenoteupdateform/{candidateNoteId}/{candidateId}", method = RequestMethod.GET)
	public String canddiateNoteUpdateForm(Model model, @PathVariable("candidateNoteId") Integer candidateNoteId,
			@PathVariable("candidateId") Integer candidateId) {

		System.out.println("Contact Note ID*********" + candidateNoteId);
		CandidateNote candidateNote = candidateNoteService.getCandidateNoteByCandidateNoteId(candidateNoteId);

		model.addAttribute("candidateNote", candidateNote);
		model.addAttribute("candidate", candidateService.getByCandidateId(candidateId));
		return "updatecandidatenoteform";
	}
  
	@RequestMapping(value = "/addcandidatenote", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addcandidateNote(Model model, @RequestParam(value = "candidateId") Integer candidateId, 
			CandidateNote candidateNote) {
		model.addAttribute("candidateNote", candidateNote);
		System.out.println("created by value is" +candidateNote.getCreatedBy());
		System.out.println("addcandidatenote");
		//newly added
		candidateNote.setStatus(true);
		candidateNoteService.save(candidateId, candidateNote);
		return "redirect:/getallcandidates";

	}

	@RequestMapping(value = "/updatecandidatenote", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String updateCandidateNote(Model model, @RequestParam(value = "candidateNoteId") Integer candidateNoteId,
			@RequestParam(value = "candidateId") Integer candidateId, CandidateNote candidateNote) {

		System.out.println("Entered Update updatecandidatenote Controller");

		System.out.println("Candidate Creted By" + candidateNote.getCreatedBy());
		System.out.println("Candidate Note ID" + candidateNoteId);
		candidateNoteService.update(candidateNoteId, candidateNote);

		Candidate candidate = candidateService.getByCandidateId(candidateId);
		model.addAttribute("candidatenotes", candidate.getCandidateNote());
		model.addAttribute("candidate", candidate);
		if (candidate.getCandidateNote().size() > 0)
			return "candidatenotelist";
		else
			return "redirect:/getallcandidates";

	}
	@RequestMapping(value = "/getallcandidatenotes", method = RequestMethod.GET)
	public String getCandidateNoteDetails(Model model) {
		model.addAttribute("candidatenotes", candidateNoteService.allCandidateNotes());
		if (candidateNoteService.allCandidateNotes().size() > 0)
			return "candidatenoteform";
		else
			return "redirect:/getallcandidates";

	}

	
	@RequestMapping(value = "/getallcandidatenotes/{candidateId}", method = RequestMethod.GET)
	public String getCandidateNoteDetails(Model model, @PathVariable("candidateId") Integer candidateId) {
		Candidate candidate = candidateService.getByCandidateId(candidateId);
		Set<CandidateNote> candidateNotes = candidate.getCandidateNote();

		Set<CandidateNote> notes= null;
		if(candidateNotes.size()>0) {
			System.err.println("-----------------------------------------------------"+ candidateNotes.size());
		for(CandidateNote candidateNote: candidateNotes) {
			if(candidateNote.getStatus()) {
				notes.add(candidateNote);

			}
		}
		}
		model.addAttribute("candidatenotes", notes);
		model.addAttribute("candidate", candidate);
		if (candidate.getCandidateNote().size() > 0)
			return "candidatenotelist";
		else
			return "redirect:/getallcandidates";
	}

	@RequestMapping(value = "/deletecandidatenote/{candidateNoteId}", method = RequestMethod.GET)
	public String deletecandidateNote(Model model, @PathVariable("candidateNoteId") Integer candidateNoteId)
			throws IOException {
		System.out.println("Entered Delete Controller");
		CandidateNote candidateNote = candidateNoteService.getCandidateNoteByCandidateNoteId(candidateNoteId);
		Candidate candidate = candidateNote.getCandidate();
		candidateNote.setStatus(false);
		candidateNoteService.update(candidateNote.getCandidateNoteId(),candidateNote);
		model.addAttribute("candidatenotes", candidateNoteService.allCandidateNotes());
		model.addAttribute("candidate", candidate);
		System.out.println("status of candidate is" +candidateNote.getStatus());
		if (candidateNoteService.allCandidateNotes().size() > 0)
			return "candidatenotelist";
		else
			return "redirect:/getallcandidates";
	}
}