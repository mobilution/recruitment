package com.mobilution.recruitment.candidate;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import com.mobilution.recruitment.job.Job;

@Entity
@Table(name = "candidate")
public class Candidate {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "candidate_id")
	private int candidateId;

	@Column(name = "candidate_name", length = 500)
	private String candidateName;

	@Column(name = "candidate_phone_no", length = 500)
	private String candidatePhoneNo;

	@Column(name = "candidate_email", length = 500)
	private String candidateEmail;

	@Column(name = "candidate_title", length = 500)
	private String candidateTitle;

	@Column(name = "recruiter_id", length = 40)
	private String recruiterId;

	@Column(name = "resume_id", length = 30)
	private String resumeId;

	@Column(name = "status", length = 50)
	private String status;

	@OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
	private Set<CandidateNote> candidateNote;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "job_candidate", joinColumns = @JoinColumn(name = "candidate_fk", referencedColumnName = "candidate_id"), inverseJoinColumns = @JoinColumn(name = "job_fk", referencedColumnName = "job_id"))
	private Set<Job> jobs;

	public Candidate() {

	}

	public Candidate(int candidateId, String candidateName, String candidatePhoneNo, String candidateEmail,
			String candidateTitle, String recruiterId, String resumeId, String status) {
		super();
		this.candidateId = candidateId;
		;
		this.candidateName = candidateName;
		this.candidatePhoneNo = candidatePhoneNo;
		this.candidateEmail = candidateEmail;
		this.candidateTitle = candidateTitle;
		this.recruiterId = recruiterId;
		this.resumeId = resumeId;
		this.status = status;
	}

	public Candidate(int candidateId, String candidateName, String candidatePhoneNo, String candidateEmail,
			String candidateTitle, String recruiterId, String resumeId, String status, Set<Job> jobs) {
		super();
		this.candidateId = candidateId;

		this.candidateName = candidateName;
		this.candidatePhoneNo = candidatePhoneNo;
		this.candidateEmail = candidateEmail;
		this.candidateTitle = candidateTitle;
		this.recruiterId = recruiterId;
		this.resumeId = resumeId;
		this.status = status;
		this.jobs = jobs;
	}

	public Candidate(String candidateName, String candidatePhoneNo, String candidateEmail, String candidateTitle,
			String recruiterId, String resumeId, String status) {
		super();

		this.candidateName = candidateName;
		this.candidatePhoneNo = candidatePhoneNo;
		this.candidateEmail = candidateEmail;
		this.candidateTitle = candidateTitle;
		this.recruiterId = recruiterId;
		this.resumeId = resumeId;
		this.status = status;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getRecruiterId() {
		return recruiterId;
	}

	public void setRecruiterId(String recruiterId) {
		this.recruiterId = recruiterId;
	}

	public String getResumeId() {
		return resumeId;
	}

	public void setResumeId(String resumeId) {
		this.resumeId = resumeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public String getCandidatePhoneNo() {
		return candidatePhoneNo;
	}

	public void setCandidatePhoneNo(String candidatePhoneNo) {
		this.candidatePhoneNo = candidatePhoneNo;
	}

	public String getCandidateEmail() {
		return candidateEmail;
	}

	public void setCandidateEmail(String candidateEmail) {
		this.candidateEmail = candidateEmail;
	}

	public String getCandidateTitle() {
		return candidateTitle;
	}

	public void setCandidateTitle(String candidateTitle) {
		this.candidateTitle = candidateTitle;
	}

	public Set<CandidateNote> getCandidateNote() {
		return candidateNote;
	}

	public void setCandidateNote(Set<CandidateNote> candidateNote) {
		this.candidateNote = candidateNote;
	}

	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

}