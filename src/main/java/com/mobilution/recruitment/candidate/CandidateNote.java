package com.mobilution.recruitment.candidate;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.type.descriptor.sql.TinyIntTypeDescriptor;

@Entity
@Table(name = "candidatenote")
public class CandidateNote {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "candidate_note_id")
	private int candidateNoteId;

	@Column(name = "text", length = 50)
	private String text;

	@Column(name = "created_by", length = 500)
	private String createdBy;

	@Column(name = "created_time", length = 40)
	private String createdTime;

	@ManyToOne
	@JoinColumn(name = "candidate_contactnote_id")
	private Candidate candidate;
	
	@Column(name = "status",nullable = false, columnDefinition = "TINYINT default 1")
	
	private Boolean status;
	
	
	public CandidateNote() {

	}

	public CandidateNote(String text, String createdBy, String createdTime,Boolean status,Candidate candidate) {
		super();

		this.text = text;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.candidate = candidate;
		this.status=status;
	}

	public int getCandidateNoteId() {
		return candidateNoteId;
	}

	public void setCandidateNoteId(int candidateNoteId) {
		this.candidateNoteId = candidateNoteId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}
}