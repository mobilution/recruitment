package com.mobilution.recruitment.candidate;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateNoteRepository extends CrudRepository<CandidateNote, Integer>{
	
	@Query("select candidateNote from CandidateNote candidateNote where candidateNote.status= 1")
	List<CandidateNote> findByStatus();
	
	void deleteByCandidateNoteId(Integer candidateNoteId);	
	
	CandidateNote getCandidateNoteByCandidateNoteId(Integer candidateNoteId);
	
	/*@Query("select c.status from candidatenote c where c.status = 1")
	CandidateNoteRepository findByStatusId(@Param("status") Boolean status);*/
}