package com.mobilution.recruitment.candidate;

import java.util.List;

import org.springframework.data.repository.CrudRepository;



public interface CandidateRepository extends CrudRepository<Candidate, Integer>{
	
	List<Candidate> findAll();
	
	void deleteByCandidateId(Integer candidateId);	
	
	Candidate getByCandidateId(Integer candidateId);
	
	void deleteCandidateNoteByCandidateId(Integer candidateId);
	
	//List<Candidate> findByCandidateNotIn(Job job);

}
