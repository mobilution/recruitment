package com.mobilution.recruitment.candidate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CandidateNoteService {

	@Autowired
	private CandidateNoteRepository candidateNoteRepository;

	@Autowired
	private CandidateRepository candidateRepository;

	public void save(CandidateNote candidateNoteDetails) {
		candidateNoteRepository.save(candidateNoteDetails);
	}

	public void save(Integer candidateId, CandidateNote candidateNoteDetails) {

		Candidate candidate = candidateRepository.getByCandidateId(candidateId);

		Set<CandidateNote> candidateNoteAs = new HashSet<CandidateNote>() {
			{
				add(new CandidateNote(candidateNoteDetails.getText(), candidateNoteDetails.getCreatedBy(),getTimeStamp(),candidateNoteDetails.getStatus(),
						 candidate));
			}
		};

		candidate.setCandidateNote(candidateNoteAs);

		candidateRepository.save(candidate);

	}

	public String getTimeStamp() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		String frmtdDate = dateFormat.format(date);

		return frmtdDate;

	}
	
	public void update(int candidateId, CandidateNote candidateNoteDetails) {
		candidateNoteRepository.save(candidateNoteDetails);
	}

	public List<CandidateNote> allCandidateNotes() {
		List<CandidateNote> notes = candidateNoteRepository.findByStatus();
		System.err.println(notes);
		return notes;
	}

	/*public void deleteByCandidateNoteId(Candidate candidate) {
		candidateNoteRepository.save(candidate);
	}*/
	
	
	public CandidateNote getCandidateNoteByCandidateNoteId(Integer candidateNoteId) {
		return candidateNoteRepository.getCandidateNoteByCandidateNoteId(candidateNoteId);
	}
	/*//newly created
	public List<CandidateNote> getCandidateNoteBystatus(Boolean status) {
  
		return candidateNoteRepository.findByStatusId(status);
	}*/
}