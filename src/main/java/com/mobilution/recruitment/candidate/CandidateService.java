package com.mobilution.recruitment.candidate;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobilution.recruitment.job.Job;

@Service
@Transactional
public class CandidateService {

	@Autowired
	private CandidateRepository candidateRepository;

	public void save(Candidate candidateDetails) {
		candidateRepository.save(candidateDetails);
	}

	public void update(int candidateId, Candidate candidateDetails) {
		candidateRepository.save(candidateDetails);
	}

	public List<Candidate> allCandidates() {
		return candidateRepository.findAll();
	}

	/*
	 * public List<Candidate> findByCandidateNotIn(Job job) { return
	 * candidateRepository.findByCandidateNotIn(job); }
	 */

	public void deleteCandidateJobs(Integer candidateId) {

		Candidate candidate = candidateRepository.getByCandidateId(candidateId);

		Set<Job> jobAs = candidate.getJobs();

		jobAs.clear();

		candidate.setJobs(jobAs);

		candidateRepository.save(candidate);

	}

	public void deleteByCandidateId(Integer candidateId) {
		candidateRepository.deleteCandidateNoteByCandidateId(candidateId);
	}

	public Candidate getByCandidateId(Integer candidateId) {
		return candidateRepository.getByCandidateId(candidateId);
	}
}