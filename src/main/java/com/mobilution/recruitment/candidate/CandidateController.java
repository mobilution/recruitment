package com.mobilution.recruitment.candidate;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
@Controller
public class CandidateController {

	@Autowired
	private CandidateService candidateService;
	
	

	@RequestMapping(value = "/candidateview", method = RequestMethod.GET)
	public String candidateView(Model model) {
		System.out.println("entered the candidate method");
		model.addAttribute("candidate", new Candidate());
		return "redirect:/getallcandidates";
	}

	@RequestMapping(value = "/addcandidate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addcandidate(Model model, Candidate candidate) {
		try {
			System.out.println("Entered Add Controller");
            candidateService.save(candidate);
            return "redirect:/getallcandidates";
		} catch (Exception e) {
			model.addAttribute("candidate", new Candidate());
            model.addAttribute("candidates", candidateService.allCandidates());
			model.addAttribute("error", "The Mobile Number or Email is already exists!");
			return "candidateform";
			
		}
	}

	@RequestMapping(value = "/candidateupdateform/{candidateId}", method = RequestMethod.GET)
	public String canddiateUpdateForm(Model model, @PathVariable("candidateId") Integer candidateId) {
	
			System.out.println("Entered candidateupdateform Controller");
			model.addAttribute("candidate", candidateService.getByCandidateId(candidateId));
			return "updatecandidateform";
	}

	@RequestMapping(value = "/getallcandidates", method = RequestMethod.GET)
	public String getClentDetails(Model model) {
		model.addAttribute("candidate", new Candidate());
		model.addAttribute("candidates", candidateService.allCandidates());
		return "candidateform";
	}

	@RequestMapping(value = "/updatecandidate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String updateCandidateNote(Model model, @RequestParam(value = "candidateId") Integer candidateId,
			Candidate candidate) {
		try {
			System.out.println("Entered updatecandidate Controller");
            candidateService.update(candidateId, candidate);
            return "redirect:/getallcandidates";
		} catch (Exception e) {
			model.addAttribute("candidate", candidateService.getByCandidateId(candidateId));
			model.addAttribute("error", "The Mobile Number or Email is already exists!");
			return "updatecandidateform";
		}
	}

	@RequestMapping(value = "/deletecandidate/{candidateId}", method = RequestMethod.GET)
	public String deletecandidate(Model model, @PathVariable("candidateId") Integer candidateId) throws IOException {
		System.out.println("Entered Delete Controller");
		
		candidateService.deleteCandidateJobs(candidateId);
		candidateService.deleteByCandidateId(candidateId);

		model.addAttribute("candidate", new Candidate());
		model.addAttribute("candidate", candidateService.allCandidates());

		return "redirect:/getallcandidates";

	}
}